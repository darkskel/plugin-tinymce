/**
 * Created by Remy Gaffard on 07/11/2015.
 * custom_emoticon
 */
function selectImg(e) {
    $(".image").css({"border": "","padding": ""}) ;
    $(".image").removeClass("selected") ;
    $(e).css({"border": "1px solid blue","padding": "4px"});
    $(e).addClass("selected") ;
}
tinymce.PluginManager.add('custom_emoticon', function(editor, url) {
    // create list of img
    var script = "";
    for(var i in tinyMCE.activeEditor.settings.custom_emoticon_list){

        script += '<img onclick="selectImg(this)" class="image" alt="'+tinyMCE.activeEditor.settings.custom_emoticon_list[i].title+'" src="'+
        tinyMCE.activeEditor.settings.custom_emoticon_list[i].value+'"/>';
    }
    editor.addButton('custom_emoticon', {
        icon: true,
        image : url + '/img/smile.png',
        onclick: function() {
            // Open window
            editor.windowManager.open({
                title: 'Emoticon',
                width : 500,
                height : 500,
                body: [
                    {type: 'container', name: 'Emoticon',
                        html   : '<style>.image {margin:5px;padding: 5px; max-height:100px;float: left;}</style>' +
                        '<div id="emoticon" style="width: 458px; height: 458px;overflow: auto;"><h2>Choose your emoticon</h2>'+script+'</div>'
                    }
                ],

                onsubmit: function(e) {
                    // submit choose
                    var src = ""+$(".selected").attr("src");
                    var alt = ""+$(".selected").attr("alt");
                    if(src !== '' && src !== 'undefined')
                        editor.insertContent('<img alt="emoticon" src="'+ src+'" alt="'+ alt+'"/>');
                    else
                        tinyMCE.activeEditor.windowManager.alert('Choose your emoticon.');

                }
            });
        }
    });


    // Adds a menu item to the tools menu
    editor.addMenuItem('custom_emoticon', {
        text: 'Emoticon',
        context: 'tools',
        onclick: function() {
            // Open window
            editor.windowManager.open({
                title: 'Emoticon',
                width : 500,
                height : 500,
                body: [
                    {type: 'container', name: 'Emoticon',
                        html   : '<style>.image {margin:5px;padding: 5px; max-height:100px;float: left;}</style>' +
                        '<div id="emoticon" style="width: 458px; height: 458px;overflow: auto;"><h2>Choose your emoticon</h2>'+script+'</div>'
                    }
                ],

                onsubmit: function(e) {
                    // submit choose
                    var src = ""+$(".selected").attr("src");
                    var alt = ""+$(".selected").attr("alt");
                    if(src !== '' && src !== 'undefined')
                        editor.insertContent('<img alt="emoticon" src="'+ src+'" alt="'+ alt+'"/>');
                    else
                        tinyMCE.activeEditor.windowManager.alert('Choose your emoticon.');

                }
            });
        }
    });
});