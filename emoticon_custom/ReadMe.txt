For install this plugin put folder "custom_emoticon_vX" in folder plugin of TinyMCE

For use this plugin you must add in the tinymce.init 3 data
The name of plugin in "plugins:["custom_emoticon"]"
The name of shortcut "toolbar:["custom_emoticon"]"
The list of emoticon " custom_emoticon_list: [
            {title: 'Name_emoticone',      value: 'http://mySuperWebsite/myemote.gif'},
            {title: 'Name_emoticone',       value: 'http://mySuperWebsite/myemote.png'},
	    {title: 'Name_emoticone',       value: 'http://mySuperWebsite/myemote.jpg'}
        ]"

Example:
    tinymce.init({
        selector: "#texte",
        height: 300,
        plugins: ["custom_emoticon otherPlugin"],
        toolbar: "custom_emoticon | otherShortcut",
        custom_emoticon_list: [
            {title: 'Name_emoticone',      value: 'http://mySuperWebsite/myemote.gif'},
            {title: 'Name_emoticone',       value: 'http://mySuperWebsite/myemote.png'},
	    {title: 'Name_emoticone',       value: 'http://mySuperWebsite/myemote.jpg'}
        ]
    });